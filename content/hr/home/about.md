---
title: "O meni"
image: "profile.jpg"
weight: 0

resources:
- src: 'resume/AK.pdf'
  name: Instruction Guide
  params:
    ref: '90564568'
---
Dobar dan, ja sam Amir,
programski inženjer s više od 15 godina iskustva u razvoju programskih rješenja.
Nakon nekoliko propalih pokušaja s Geodezijom, završio sam računalno programiranje 2005 na FESB-u.

{{< manifest "/resume/Ak.pdf" "CV" >}}

Nakon toga sam promijeno nekoliko manjih IT firmi te sam se zaposlio u RBA Austria d.d.

Nakon 12 godina odlučio sam da je došlo vrijeme za neke nove početke.

Moje iskustvo je u najvećoj mjeri temeljeno na razvoju i integraciji programskih rješenja napisanih u Java programskom jeziku u tehnolgijama kao što su:

- Spring
- Hibernate
- Struts
- JSF

Veliki sam poklonik kontinuiranog build/test razvoja s alatima kao što su:

- Jenkins
- Maven
- Ant

Prema potrebi spreman na skok u bespuće JS biblioteka kao što su:

- NodeJS
- Jquery
- React

Cloud tehnolgije

- AWS
- Docker

