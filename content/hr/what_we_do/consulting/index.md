---
title: "Konzultacije"
weight: 3
---

Znate kuda želite stići, ali
ne znate od kuda bi ste krenuli, ili znate od kuda bi ste krenuli ali ne 
znate u kojem smjeru.

Obratite se meni :)
Volio bih poslušati vaše ideje za unaprijeđenje vlastitog posla.