---
title: "Razvoj software-a"
weight: 2

---
Full stack development

- JEE  (Java enterprise edition), Spring, Hibernate
- Javascript, React, Angular
- HTML, CSS, SCSS 
- Cloud tehnologije (AWS,Docker)
