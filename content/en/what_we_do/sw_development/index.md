---
title: "Software development"
weight: 2 
resources:
    - src: puzzle.jpg
      params:
          weight: -100
---

## SERVER-SIDE DEVELOPMENT
Server-side development can vary in complexity, but that is not something you should worry about as our client. 
It can be simple as a CRUD API or complex as scalable could application. 
If you need to make a server-side system from scratch or you need to extends current one, we are your team. 

#### We like challenges, technologies are no obstacle for us!

We can handle any leading server-side technology (.NET, Java, NodeJS, Go, … you name it)
work with any leading database system (MySql, PostgreSQL, Microsoft SQL Server, MongoDB)
we can deploy server components on cloud infrastructure or your machines
we have experience working with containerized systems (Docker, Kuberenetes, AWS)


## CONTAINER BASED DEVELOPMENT
Containers are great new approach in software development. 
They tend to make the whole process of development > testing > delivery > repeat whole lot easier. 
With container based approach you can make complex systems achievable.

* we work with technologies such as Docker and Kubernetes
* we can apply them to your choice of server side technology

## FRONT-END DEVELOPMENT
Either website or singe-page-app, our engineers can take any idea and transform it into blasting web interface. We keep ourselves on the edge of front-end technologies.

* we support leading front-end trends HTML5, CSS3, SASS
* our Javascript developers are crafted in web frameworks like Angular, ReactJS, Aurelia, …

## WEB AND API DEVELOPMENT
Connecting two worlds (server-side and front-end) is like building a bridge starting from both sides and has to perfectly match in the middle. Get our team involved and make great APIs or Web apps. Make your existing systems integrate with each other. Get in charge of your data and use it to your advantage

* we can handle any leading server-side technology (.NET, Java, NodeJS, Go, … you name it)
* integration with existing services or creating new ones
* use of leading databases (MySql, PostgreSQL, Microsoft SQL Server, MongoDB)

## CLOUD DEVELOPMENT
Cloud development is development taken to another level where apps need to scale and adopt according to traffic and user load. 
Build your apps so they can scale and still be successful when your number of clients reach high number. 
If you don’t know how to handle it by yourself get our team involved and relax. We have lots of experience working with cloud infrastructures.

* scale apps according to your needs
* avoid unnecessary expenses for resource hat you don’t use
* avoid downtime with system that can repair itself
* working with Amazon, Azure and Google could services
* working with containerized systems to make cloud delivery achievable
